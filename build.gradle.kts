plugins {
    id("java")
	kotlin("jvm") version "1.7.10"
}

repositories {
    mavenCentral()
}

dependencies {
	implementation( kotlin("stdlib-jdk8") )
}

typealias KotlinCompile = org.jetbrains.kotlin.gradle.tasks.KotlinCompile
val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
	jvmTarget = "1.8"
}
val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
	jvmTarget = "1.8"
}